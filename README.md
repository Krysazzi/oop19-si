# README #

Il gruppo si pone di realizzare un gioco con vista 2D a scorrimento verticale dove si prende il controllo di una navicella che si può spostare nello spazio di gioco tramite movimento del mouse.

L’obiettivo è raggiungere la fine del livello, durante il quale il giocatore si troverà contro diverse ondate di nemici.

Se si viene colpiti troppo si dovrà ricominciare il livello da capo.

### Funzionalità minime ritenute obbligatorie: ###

* Menu principale all’avvio dell’applicazione (Avvia, Comandi, Crediti);
* Controllo del giocatore tramite mouse;
* Nemici che si muovono tramite pattern predefinito o seguendo la posizione del giocatore;
* Possibilità di sparare per nemici e giocatore;
* Boss Fight; 
* Barra della vita (barra o cuori);
* Numero di vite;
* Livelli ad ondate;
* Diversi tipi di nemici;
* Leaderboard (Punteggi).


### Funzionalità opzionali: ###

* Modalità Endless;
* Salvataggi;
* Diversi tipi di sparo per il giocatore;
* Selettore della difficoltà;
* Opzioni di input;
* PowerUps;
* Scaling della schermata;

### "Challenge" Principali: ###

* Gestire il comportamento tra il game engine e l’interfaccia grafica;
* Gestione delle interazioni tra le entità;
* Gestione delle risorse.

