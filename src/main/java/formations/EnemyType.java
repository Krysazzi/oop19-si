package formations;

/**
 * An enum used to indicate the type of a single enemy that 
 * should be in an EnemyFormation.
 */
public enum EnemyType {
    /**
     * An Enum value indicating the enemy type "Drone".
     */
    DRONE,
    /**
     * An Enum value indicating the enemy type "Turret".
     */
    TURRET,
    /**
     * An Enum value indicating the enemy type "Muncher".
     */
    MUNCHER,
    /**
     * An Enum value indicating the enemy type "Boss".
     */
    BOSS;
}
